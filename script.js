var btn = document.getElementById("btnBusca")
var results = document.getElementById
("resultado")
var resultado
btn.addEventListener("click", async function(){
    buscaPalavra()
})

document.addEventListener('keydown', function(e) {
    if(e.key == "Enter"){
        buscaPalavra();
    }
});

async function buscaPalavra(){
    let palavra = document.getElementById("palavra").value
    var xhr = new XMLHttpRequest()
    changeColor()
    xhr.open("GET", "https://api.dicionario-aberto.net/word/"+palavra)
    
    xhr.addEventListener("load", function () {
        var resposta = xhr.responseText
        resultado = JSON.parse(resposta)
        results.innerHTML=resultado[0].xml;
    })
    
    xhr.send()
    
}

function changeColor(){
    results.style.backgroundColor = "#95afc0"
    results.style.borderRadius = "10px"
}